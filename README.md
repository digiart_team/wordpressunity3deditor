# README #

WordpressUnity3DEditor 

A Wordpress plugin for editing remotely Unity3D projects. 

### What is this repository for? ###

* This code helps someone to build a Unity3D game remotely without writting any code. It exploits the pros of Wordpress to allow drag'n'drop actions to make a scene and play it. 

v.Alpha 23_02_2017


### How do I get set up? ###

a) You need to have a WordPress site (4.7 or later) and Unity3D should be installed according to the following instructions:

A Server operating system with Ubuntu 16 or Windows 10 is required. The server software XAMPP with php 5.6 for Ubuntu or Windows should be used which is public available . WordPress 4.5 should be also installed which is also public available . XAMPP and WordPress are not developed in DigiArt and therefore there is no testing plan for them here. However, they should be installed and tested according to their vendors. If any modifications on their files are required to comply with DigiArt components, it will be explicitly stated.

### UNITY INSTALLATION AT UBUNTU (LINUX) SERVER SYSTEM ###

Download, install, run, and activate license Unity3D 5.5.0p1 for Ubuntu. Instructions are available at the Unity website and specifically in the forum dedicated for the Linux version . The expected behavior is to see the Graphic User Interface (GUI) of Unity3D editor. Unexpected behavior is a gray screen without loading anything. This is due to the wrong version used. Beta versions should not be used.

### UNITY INSTALLATION TESTING AT WINDOWS 10 SERVER SYSTEM ###
Download Unity3D free version 5.5 for Windows from Unity3D website . Install, run and activate it in the windows that will appear. The GUI of Unity3D should be loaded, otherwise an error will be prompted.


Then you download the whole repository as a zip and install it from plugins->add new->upload zip


### Contribution guidelines ###

* Writing tests: To appear
* Code review: To appear
* Other guidelines: To appear

### Who do I talk to? ###

* I am coordinating and contributing to this repository: Dimitrios Ververidis, ververid@iti.gr, jimver04@gmail.com